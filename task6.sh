#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the creation of 
# resources in "Task 6: Create a NAT Gateway" of 
# Module7: Lab 4 - Configure VPC (AWS Cloud Operations course)

source vars.sh # load needed variables of other tasks

NAT_GATEWAY_NAME='lab4-nat-gateway'
ELASTIC_IP_NAME='NAT-gateway-eip'
PRIVATE_ROUTE_TABLE_NAME='Private-Route-Table'

# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filters "Name=tag:Name,Values=$VPC_NAME" \
                    "Name=state,Values=available" \
          --query 'Vpcs[*].VpcId' \
          --output text)

if [[ -z $vpcId ]]; then
  echo "[+] No $VPC_NAME VPC was created!"
  exit 1
fi

echo "$VPC_NAME VPC: $vpcId"

# creating an Elastic IP for the NAT gateway
echo "[*] Allocating an Elastic IP for $NAT_GATEWAY_NAME NAT gateway"
eipId=$(aws ec2 allocate-address \
        --tag-specifications "ResourceType=elastic-ip,Tags=[{Key=Name,Value=$ELASTIC_IP_NAME}]" \
        --query 'AllocationId' \
        --output text)


# Creating an NAT gateway on public subnetwork 

publicSubnetId=$(aws ec2 describe-subnets \
                  --filters "Name=tag:Name,Values=$PUBLIC_SUBNET_NAME" \
                            "Name=vpc-id,Values=$vpcId" \
                  --query 'Subnets[*].SubnetId' \
                  --output text)

natGwId=$(aws ec2 create-nat-gateway \
            --allocation-id $eipId \
            --subnet-id $publicSubnetId \
            --tag-specifications "ResourceType=natgateway,Tags=[{Key=Name,Value=$NAT_GATEWAY_NAME}]" \
            --query 'NatGateway.NatGatewayId' \
            --output text)


# Creating route table
rtbId=$(aws ec2 describe-route-tables \
          --filters "Name=vpc-id,Values=$vpcId" \
                    "Name=tag:Name,Values=$PRIVATE_ROUTE_TABLE_NAME" \
          --query 'RouteTables[*].RouteTableId' \
          --output text)

if [[ -z $rtbId ]];then
  echo "[+] Creating $PRIVATE_ROUTE_TABLE_NAME route table"
  rtbId=$(aws ec2 create-route-table \
          --vpc-id $vpcId \
          --tag-specifications "ResourceType=route-table,Tags=[{Key=Name,Value=$PRIVATE_ROUTE_TABLE_NAME}]" \
          --query 'RouteTable.RouteTableId' \
          --output text)
fi

echo "$PRIVATE_ROUTE_TABLE_NAME route table: $rtbId"


# Create internet route to attach to private route table

echo "[*] Adding internet route to $PRIVATE_ROUTE_TABLE_NAME route table"
aws ec2 create-route \
  --destination-cidr-block 0.0.0.0/0 \
  --route-table-id $rtbId \
  --gateway-id $natGwId

# Attaching route table to private subnet
privateSubnetId=$(aws ec2 describe-subnets \
                  --filters "Name=tag:Name,Values=$PRIVATE_SUBNET_NAME" \
                            "Name=vpc-id,Values=$vpcId" \
                  --query 'Subnets[*].SubnetId' \
                  --output text)
                  
echo "[*] Attaching $PRIVATE_ROUTE_TABLE_NAME route table to $PRIVATE_SUBNET_NAME subnet"
aws ec2 associate-route-table \
  --route-table-id $rtbId \
  --subnet-id $privateSubnetId