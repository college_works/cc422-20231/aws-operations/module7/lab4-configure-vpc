#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the creation of 
# resources in "Task 3: Create an Internet Gateway" of 
# Module7: Lab 4 - Configure VPC (AWS Cloud Operations course)

source vars.sh # load needed variables of other tasks

INTERNET_GATEWAY_NAME='Lab IGW'


# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filters "Name=tag:Name,Values=$VPC_NAME" \
                    "Name=state,Values=available" \
          --query 'Vpcs[*].VpcId' \
          --output text)

if [[ -z $vpcId ]]; then
  echo "[-] No $VPC_NAME VPC was created!"
  exit 1
fi

# Creating internet gateway
igwId=$(aws ec2 describe-internet-gateways \
        --filter "Name=tag:Name,Values=$INTERNET_GATEWAY_NAME" \
        --query 'InternetGateways[*].InternetGatewayId' \
        --output text)

if [[ -z $igwId ]];then
  echo "[+] Creating $INTERNET_GATEWAY_NAME internet gateway"
  igwId=$(aws ec2 create-internet-gateway \
            --tag-specifications "ResourceType=internet-gateway,Tags=[{Key=Name,Value=$INTERNET_GATEWAY_NAME}]" \
            --query 'InternetGateway.InternetGatewayId' \
            --output text)
fi

echo "Internet gateway $INTERNET_GATEWAY_NAME: $igwId"

# Attaching VPC to internet gateway
aws ec2 attach-internet-gateway \
  --internet-gateway-id $igwId \
  --vpc-id $vpcId 2> /dev/null
