#!/bin/bash

## Task 1 variables
VPC_NAME='Lab VPC'
VPC_CIDR='10.0.0.0/16'

## Task 2 variables
PUBLIC_SUBNET_NAME='Public-Subnet'
PUBLIC_SUBNET_CIRD='10.0.0.0/24'

PRIVATE_SUBNET_NAME='Private-Subnet'
PRIVATE_SUBNET_CIRD='10.0.2.0/23'

## Task3 variables
INTERNET_GATEWAY_NAME='Lab IGW'

## Task 4 variables
PUBLIC_ROUTE_TABLE_NAME='Public-Route-Table'
PUBLIC_SUBNET_NAME='Public-Subnet' # taget subnet for route table

## Task 5 variables 
INSTANCE_NAME='Bastion Server'
INSTANCE_TYPE='t2.micro'
KEY_PAIR='vockey'
SECURITY_GROUP_NAME='BastionSG'