#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the creation of 
# resources in "Task 4: Configure Route Tables" of 
# Module7: Lab 4 - Configure VPC (AWS Cloud Operations course)

source vars.sh # load needed variables of other tasks

PUBLIC_ROUTE_TABLE_NAME='Public-Route-Table'
PUBLIC_SUBNET_NAME='Public-Subnet' # taget subnet for route table

# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filters "Name=tag:Name,Values=$VPC_NAME" \
                    "Name=state,Values=available" \
          --query 'Vpcs[*].VpcId' \
          --output text)

if [[ -z $vpcId ]]; then
  echo "[+] No $VPC_NAME VPC was created!"
  exit 1
fi


# Creating public route table
rtbId=$(aws ec2 describe-route-tables \
          --filters "Name=vpc-id,Values=$vpcId" \
                    "Name=tag:Name,Values=$PUBLIC_ROUTE_TABLE_NAME" \
          --query 'RouteTables[*].RouteTableId' \
          --output text)

if [[ -z $rtbId ]];then
  echo "[+] Creating $PUBLIC_ROUTE_TABLE_NAME route table"
  rtbId=$(aws ec2 create-route-table \
          --vpc-id $vpcId \
          --tag-specifications "ResourceType=route-table,Tags=[{Key=Name,Value=$PUBLIC_ROUTE_TABLE_NAME}]" \
          --query 'RouteTable.RouteTableId' \
          --output text)
fi

echo "$PUBLIC_ROUTE_TABLE_NAME route table: $rtbId"

# Create internet route to attach to public route table
igwId=$(aws ec2 describe-internet-gateways \
        --filter "Name=tag:Name,Values=$INTERNET_GATEWAY_NAME" \
        --query 'InternetGateways[*].InternetGatewayId' \
        --output text)

if [[ -z $igwId ]];then
  echo "[-] No $INTERNET_GATEWAY_NAME internet gateway was created!!"
  exit 1
fi


echo "[*] Adding internet route to $PUBLIC_ROUTE_TABLE_NAME route table"
aws ec2 create-route \
  --destination-cidr-block 0.0.0.0/0 \
  --route-table-id $rtbId \
  --gateway-id $igwId

# Attach public route table to public subnet

publicSubnetId=$(aws ec2 describe-subnets \
                  --filters "Name=tag:Name,Values=$PUBLIC_SUBNET_NAME" \
                  --query 'Subnets[*].SubnetId' \
                  --output text)

if [[ -z $publicSubnetId ]];then
  echo "[-] No $PUBLIC_SUBNET_NAME subnet was created!!"
  exit 1
fi


echo "[*] Attaching $PUBLIC_ROUTE_TABLE_NAME route table to $PUBLIC_SUBNET_NAME subnet"
aws ec2 associate-route-table \
  --route-table-id $rtbId \
  --subnet-id $publicSubnetId