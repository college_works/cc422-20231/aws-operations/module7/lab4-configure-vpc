#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the creation of 
# resources in "Task 2: Create Subnets" of Module7: Lab 4 - Configure VPC (AWS Cloud Operations course)


source vars.sh # load needed variables of other tasks

PUBLIC_SUBNET_NAME='Public-Subnet'
PUBLIC_SUBNET_CIRD='10.0.0.0/24'

PRIVATE_SUBNET_NAME='Private-Subnet'
PRIVATE_SUBNET_CIRD='10.0.2.0/23'

# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filters "Name=tag:Name,Values=$VPC_NAME" \
                    "Name=state,Values=available" \
          --query 'Vpcs[*].VpcId' \
          --output text)

if [[ -z $vpcId ]]; then
  echo "[+] No $VPC_NAME VPC was created!"
  exit 1
fi

echo "$VPC_NAME VPC: $vpcId"

# Selecting an AZ on region (the first, just for simplicity)
az=$(aws ec2 describe-availability-zones \
      --query 'AvailabilityZones[0].ZoneName' \
      --output text)

echo "AZ: $az"


# Creating Public-Subnet

publicSubnetId=$(aws ec2 describe-subnets \
                  --filters "Name=cidr-block,Values=$PUBLIC_SUBNET_CIRD" \
                            "Name=tag:Name,Values=$PUBLIC_SUBNET_NAME" \
                  --query 'Subnets[*].SubnetId' \
                  --output text)

if [[ -z $publicSubnetId ]];then
  echo "[+] Creating $PUBLIC_SUBNET_NAME subnet"
  publicSubnetId=$(aws ec2 create-subnet \
                    --cidr-block $PUBLIC_SUBNET_CIRD \
                    --vpc-id $vpcId \
                    --tag-specifications "ResourceType=subnet,Tags=[{Key=Name,Value=$PUBLIC_SUBNET_NAME}]" \
                    --query 'Subnet.SubnetId' \
                    --output text) 
fi

echo "[*] $PUBLIC_SUBNET_NAME subnet: $publicSubnetId"

# Creating Private-Subnet

privateSubnetId=$(aws ec2 describe-subnets \
                    --filters "Name=cidr-block,Values=$PRIVATE_SUBNET_CIRD" \
                              "Name=tag:Name,Values=$PRIVATE_SUBNET_NAME" \
                    --query 'Subnets[*].SubnetId' \
                    --output text)

if [[ -z $privateSubnetId ]];then
  echo "[+] Creating $PRIVATE_SUBNET_NAME subnet"
  privateSubnetId=$(aws ec2 create-subnet \
                    --cidr-block $PRIVATE_SUBNET_CIRD \
                    --vpc-id $vpcId \
                    --tag-specifications "ResourceType=subnet,Tags=[{Key=Name,Value=$PRIVATE_SUBNET_NAME}]" \
                    --query 'Subnet.SubnetId' \
                    --output text) 
fi

echo "[*] $PRIVATE_SUBNET_NAME subnet: $privateSubnetId"


